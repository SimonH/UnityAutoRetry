
// UnityAutoRetryDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "afxwin.h"
#include "UnityAutoRetry.h"
#include "UnityAutoRetryDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CUnityAutoRetryDlg 对话框



CUnityAutoRetryDlg::CUnityAutoRetryDlg(CWnd* pParent /*=NULL*/)
: CDialogEx(CUnityAutoRetryDlg::IDD, pParent) {
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    commWatchThread = NULL;
}

bool exitDlg = false;
CUnityAutoRetryDlg::~CUnityAutoRetryDlg() {
    exitDlg = true;
    WaitForSingleObject(commWatchThread->m_hThread, INFINITE);
    if(commWatchThread != NULL) {
        delete commWatchThread;
        commWatchThread = NULL;
    }
}

void CUnityAutoRetryDlg::DoDataExchange(CDataExchange* pDX) {
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_TEXT1, retryCount);
}

BEGIN_MESSAGE_MAP(CUnityAutoRetryDlg, CDialogEx)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
END_MESSAGE_MAP()

int retrySum = 0;
UINT  CheckUnityRetry(LPVOID  lParam) {
    CUnityAutoRetryDlg* dlg = (CUnityAutoRetryDlg*)lParam;
    CString findBtn("&Try Again");
    retrySum = 0;
    while(true && !exitDlg) {
        HWND handler = ::FindWindow(NULL, _T("Opening file failed"));
        if(handler != NULL) {
            CWnd* m_hWnd = CWnd::FromHandle(handler);
            HWND hwndChild = ::GetWindow(handler, GW_CHILD); //列出所有控件
            while(hwndChild) {
                TCHAR szWndTitle[256] = { 0 };
                TCHAR szClsName[64] = { 0 };
                ::GetWindowText(hwndChild, szWndTitle, 256);
                ::GetClassName(hwndChild, szClsName, 64);
                if(findBtn == szWndTitle) {
                    retrySum++;
                    CString tip("重试次数: ");
                    tip.AppendFormat(_T("%d"), retrySum);
                    dlg->retryCount.SetWindowTextW(tip);
                    ::SendMessage(hwndChild, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(0, 0));
                    ::SendMessage(hwndChild, WM_LBUTTONUP, MK_LBUTTON, MAKELPARAM(0, 0));
                    break;
                }
                hwndChild = ::GetWindow(hwndChild, GW_HWNDNEXT);
            }
        }
        Sleep(1000);
    }
    return 1;
}

// CUnityAutoRetryDlg 消息处理程序

BOOL CUnityAutoRetryDlg::OnInitDialog() {
    CDialogEx::OnInitDialog();


    commWatchThread = AfxBeginThread(CheckUnityRetry, (LPVOID)this);//启动新的线程
    commWatchThread->m_bAutoDelete = FALSE;

    //MessageBox(_T("Test"), _T("Opening file failed1"), 0);
    // 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
    //  执行此操作
    SetIcon(m_hIcon, TRUE);         // 设置大图标
    SetIcon(m_hIcon, FALSE);        // 设置小图标

    // TODO:  在此添加额外的初始化代码

    return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CUnityAutoRetryDlg::OnPaint() {
    if(IsIconic()) {
        CPaintDC dc(this); // 用于绘制的设备上下文

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 使图标在工作区矩形中居中
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 绘制图标
        dc.DrawIcon(x, y, m_hIcon);
    } else {
        CDialogEx::OnPaint();
    }
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CUnityAutoRetryDlg::OnQueryDragIcon() {
    return static_cast<HCURSOR>(m_hIcon);
}

