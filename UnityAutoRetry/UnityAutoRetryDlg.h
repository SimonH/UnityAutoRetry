
// UnityAutoRetryDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CUnityAutoRetryDlg 对话框
class CUnityAutoRetryDlg : public CDialogEx
{
// 构造
public:
	CUnityAutoRetryDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CUnityAutoRetryDlg();

// 对话框数据
	enum { IDD = IDD_UNITYAUTORETRY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	CWinThread* commWatchThread;
public:
	CStatic retryCount;
};
